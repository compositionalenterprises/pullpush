FROM python:3.8.0-slim

ADD . /pullpush

RUN python /pullpush/setup.py install

CMD ["python", "/pullpush/pullpush.py"]


