from setuptools import setup, find_packages

setup(
    name='pullpush',
    version='1.0.0',
    description='A module made to pull docker images from gitlab and push them to dockerhub',
    author='Jack Moore',
    author_email='moorew1997@gmail.com',
    install_requires=['docker', 'gitlab'], 
    packages=find_packages()
)

