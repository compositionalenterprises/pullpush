import docker
import gitlab
import requests
import json
import os

class NoRepoError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr("It looks like you don't have a repo defined!")


def get_git_tags(repo=None):
    if repo == None: raise NoRepoError() 
    replaced_repo = repo.replace("/", "%2F")
    gitlab_tags_url = "https://gitlab.com/api/v4/projects/" + replaced_repo + "/repository/tags"
    request = requests.get(url = gitlab_tags_url)
    response = request.json()[0]['name']
    return response

def pull_and_push(client, base_repo, repo_tag, gitlab_registry_image, dockerio_registry_image):
    print("Pulling: " + base_repo)
    print("Gitlab Registry: " + gitlab_registry_image)
    for line in client.pull(gitlab_registry_image, tag=repo_tag, stream=True, decode=True):
        if line['status'] == "Pull Complete": print(line['status']) 
    print("Tagging: " + base_repo)
    client.tag(image=gitlab_registry_image + ":" + repo_tag, repository=dockerio_registry_image, tag=repo_tag)
    print("Pushing: " + base_repo)
    for line in client.push(dockerio_registry_image, tag=repo_tag, stream=True, decode=True):
        if line.get('aux') != None: print("Pushed up image with tag: " + line['aux']['Tag'])

def clean_images(client, repo_tag, gitlab_registry_image, dockerio_registry_image):
    print("Removing image from disk!")
    client.remove_image(gitlab_registry_image + ":" + repo_tag)
    client.remove_image(dockerio_registry_image + ":" + repo_tag)

def get_repos():
    repos = []
    if os.getenv("GITLAB_REPO") != None:
        repos.append(os.getenv("GITLAB_REPO"))
        return repos
    with open('repos.txt', 'r') as fp:
        for ct, line in enumerate(fp):
            repos.append(line.replace("\n", ""))
    return repos

def main():
    client = docker.APIClient(base_url='unix://var/run/docker.sock')
    if os.getenv("DOCKER_PASSWORD") == None or os.getenv("DOCKER_USERNAME") == None or os.getenv("DOCKER_EMAIL") == None: 
        print("PLEASE SET DOCKER ENVIRONMENT VARIABLES!")
        return -1 
    registry_login = client.login(username=os.getenv("DOCKER_USERNAME"), password=os.getenv("DOCKER_PASSWORD"), email=os.getenv("DOCKER_EMAIL"), registry="https://index.docker.io/v1/")

    gitlab_git_repos = get_repos()
    if len(gitlab_git_repos) == 0:
        print("PLEASE ADD A REPOS.TXT FILE OR A 'GITLAB_REPO' ENVIRONMENT VARIABLE!")
        return -1
    for repo in gitlab_git_repos:
        repo_tag = get_git_tags(repo)
        gitlab_registry_image = "registry.gitlab.com/" + repo
        dockerio_registry_image = "docker.io/" + repo.replace("-","")
        pull_and_push(client, repo, repo_tag, gitlab_registry_image, dockerio_registry_image)
        clean_images(client, repo_tag, gitlab_registry_image, dockerio_registry_image)

    print("Done!")




if __name__ == '__main__':
    main()



