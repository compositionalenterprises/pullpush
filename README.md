# PullPush.py

This script is responsible for pulling docker images generated in gitlab's image registry and pushing them to dockerhub's image registry.   
I use it because I have had tons of trouble with Gitlab's cicd pushing images out to docker's image registry.

Right now the requirements for the script to run are Python3.8 and Docker running on a local unix socket.


# Dockerfile

Just going to chuck and pray that the docker instance on most user's machines are compatible with the Python Docker SDK!
  - Docker version 19.03.5, build 633a0ea838


```sh
# Pull down latest version of the image
docker pull pullpush:latest

# Run the docker instance
docker run -e DOCKER_PASSWORD=P4$$wordHere -e DOCKER_USERNAME=Username -e DOCKER_EMAIL=example@example.com -v /var/run/docker.sock:/var/run/docker.sock -v /path/to/repos.txt:/app/repos.txt --name pullpush pullpush

# Need to update dockerhub, just restart the image
docker restart pullpush

# Need the logs?
docker logs pullpush
```

## Repos File

The `repos.txt` file should be separated line by line with each gitlab repo.

```txt
example/example1
example/example2
```


